import AppointmentServices from "./service";

const AppointmentController = {
  getAppointmentByUser: (req, res) => {
    AppointmentServices.getAppointmentByUser(req, result => {
      result.success ? res.status(200).send(result) : res.status(404).send(result)
    })
  },

 
  confirm: async (req, res) => {
    AppointmentServices.confirm(req.body).then(result =>
      res.status(result.status).send(result.payload)
    );
  },

   cancel: (req, res) => {
        AppointmentServices.cancel(req, result => {
            result.success ? res.status(200).send(result) : res.status(404).send(result)
        })
    },


getHoursByVet: (req, res) => {
		AppointmentServices.getAppointsByVet(req, (result) => {
			result.success
				? res.status(200).send(result)
				: res.status(404).send(result);
		});
  },

  getAppointsByUser: (req, res) => {
		AppointmentServices.getAppointsByUser(req, (result) => {
			result.success
				? res.status(200).send(result)
				: res.status(404).send(result);
		});
  },
  
   
   
   

};

export default AppointmentController;
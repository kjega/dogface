import db, { database } from "../../setup/database";

const Queries = {


	getAppointsByVet: (req, successCallback, failureCallback) => {
        console.log(req.params);
      
        //appointments, veterinary (select from both)  (common key - id_vet)
        //2(id_vet), 3 (id_vet)   NET rows
        //INNER JOIN (join the two tables usiong common key(column))
        //a, v , u --  aliases for the tables
        //replace '*' from query, with necessary columns only!! ( like a.id_user etc)
    let sqlQuery =
          `SELECT * FROM appointments a INNER JOIN veterinary v ON a.id_vet=v.id_vet INNER JOIN users u ON u.id_user=v.id_user WHERE  u.is_veterinay=1 AND  a.id_vet=${req.params.id_vet}`
		db.query(sqlQuery, (err, rows) => {
			if (err) {
				return failureCallback(err);
			}
			if (rows.length > 0) {
				return successCallback(rows);
			} else {
				return successCallback(rows);
			}
		});
	},
  

    getAppointsByUser: (req, successCallback, failureCallback) => {
        console.log(req.params);
               //join for DOG table also (using id_dog)
    let sqlQuery =
          `SELECT * FROM appointments a INNER JOIN veterinary v ON a.id_vet=v.id_vet INNER JOIN users u ON u.id_user=v.id_user INNER JOIN dogs d ON d.id_dog=a.id_dog WHERE  a.id_user=${req.params.id_user}`
		db.query(sqlQuery, (err, rows) => {
			if (err) {
				return failureCallback(err);
			}
			if (rows.length > 0) {
				return successCallback(rows);
			} else {
				return successCallback(rows);
			}
		});
	},
  


};

export default Queries;


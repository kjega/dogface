import express from "express";
import AppointmentController from "./controller";

const router = express.Router();

router.get("/:id_vet", AppointmentController.getHoursByVet);
router.get("/user/:id_user", AppointmentController.getAppointsByUser);


export default router;
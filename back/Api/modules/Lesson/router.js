import express from "express";
import LessonController from "./controller";

const router = express.Router();


router.get("/", LessonController.getAllLessons);
router.get("/:id", LessonController.getAllStepsByIdLess);







export default router;

import db, { database } from "../../setup/database";



const Queries = {

  
  getAllStepsByIdLess: (param, successCallback, failureCallback) => {
    let sqlQuery = `SELECT * FROM step_lesson WHERE id_lesson=${param.params.id}`;
    db.query(sqlQuery, (err, rows) => {
      if (err) {
        return failureCallback(err);
      }
      if (rows.length > 0) {
        return successCallback(rows);
      } else {
        return successCallback("No matching lesson");
      }
    })
  },
  
 getAllLessons: (param, successCallback, failureCallback) => {
    let sqlQuery = `SELECT * FROM lessons`;
    db.query(sqlQuery, (err, rows) => {
      if (err) {
        return failureCallback(err);
      }
      if (rows.length > 0) {
        return successCallback(rows);
      } else {
        return successCallback("No matching lesson");
      }
    })
  },

// getLessonByIdPack: (param, successCallback, failureCallback) => {
//     let sqlQuery = `SELECT * FROM lesson WHERE id_package=${param.params.id}`;
//     db.query(sqlQuery, (err, rows) => {
//       if (err) {
//         return failureCallback(err);
//       }
//       if (rows.length > 0) {
//         return successCallback(rows);
//       } else {
//         return successCallback("No matching lesson");
//       }
//     })
//   },

};


export default Queries;

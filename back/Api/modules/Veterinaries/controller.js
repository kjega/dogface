import VetServices from "./service";
// let Queries = require('./query');

// exports.getHoursByVet = (req, res, next) => {
//   // const id = +req.params.id;
//   const id = +req.body.id;

//   console.log("ID : ", id);
//   Queries.getVetByAvail(+id).then(result => {
//     console.log(result);
//     res.json({ message: "Data returned", status: true });
//   })
//   .catch(err=>console.log(err))

// }

const VetController = {
  getVet: (req, res) => {
    VetServices.getVet(req, result => {
      result.success ? res.status(200).send(result) : res.status(404).send(result)
    })
  },
  authenticate: (req, res) => {
    VetServices.authenticate(req.body).then(result =>
      res.status(result.status).send(result.payload)
    );
  },
 
  register: async (req, res) => {
    VetServices.register(req.body).then(result =>
      res.status(result.status).send(result.payload)
    );
  },

   delete: (req, res) => {
        VetServices.delete(req, result => {
            result.success ? res.status(200).send(result) : res.status(404).send(result)
        })
    },

editVet: (req, res) => {
   
    VetServices.editVet(req, (result) => {
      result.success
        ? res.status(200).send(result)
        : res.status(404).send(result);
    });
  },
  getAllVet: (req, res) => {
    VetServices.getAllVet(req, result => {
      result.success ? res.status(200).send(result)
        : res.status(404).send(result);
    })
  },

getHoursByVet: (req, res) => {
		VetServices.getHoursByVet(req, (result) => {
			result.success
				? res.status(200).send(result)
				: res.status(404).send(result);
		});
	},

  takeAppointment: (req, res) => {
    VetServices.takeAppointment(req, (result) => {
      result.success
        ? res.status(200).send(result)
        : res.status(404).send(result);
    });
  },
  
 cancelAppointment: (req, res) => {
    VetServices.cancelAppointment(req, (result) => {
      result.success
        ? res.status(200).send(result)
        : res.status(404).send(result);
    });
  }
 
};

export default VetController;

import db, { database } from "../../setup/database";

const Queries = {


	getAllVet: (req, successCallback, failureCallback) => {
		console.log("cccccccc"+ req.params);
    let sqlQuery =
       `SELECT * FROM users INNER JOIN veterinary v ON users.id_user=v.id_user WHERE users.is_veterinay = 1`
		db.query(sqlQuery, (err, rows) => {
			if (err) {
				return failureCallback(err);
			}
			if (rows.length > 0) {
				return successCallback(rows);
			} else {
				return successCallback();
			}
		});
	},


	getHoursByVet:(req, successCallback, failureCallback) => {
		console.log(req.params);
    let sqlQuery =
          `SELECT * FROM veterinary_has_hour WHERE  available = 1 AND  id_vet=${req.params.id}`
		db.query(sqlQuery, (err, rows) => {
			if (err) {
				return failureCallback(err);
			}
			if (rows.length > 0) {
				return successCallback(rows);
			} else {
				return successCallback(rows);
			}
		});
	},

   createAppoint: async (appointData) => {
    console.log("Appoint Data : ",appointData);
      return new Promise((resolve, reject) => {
      let sqlQuery = `INSERT INTO appointments (id_appointment,id_user, id_dog,id_vet_hour, motif,id_vet) VALUES (NULL,"${appointData.id_user}", "${appointData.id_dog}", "${appointData.id_vet_hour}", "${appointData.motif}", "${appointData.id_vet}")`;
       db.query(sqlQuery, (err, res) => {
         if (err) reject(err);
        //  console.log("ID_USER :", res.insertId);
       
         resolve(res);
      });
    });
  },

  


  takeAppointment: (param, successCallback, failureCallback) => {  
    console.log(param);
    param.id_vet_hour = +param.id_vet_hour;
      let sqlQuery = `UPDATE veterinary_has_hour SET available=0 WHERE id_vet_hour=${param.id_vet_hour}`;
    console.log(sqlQuery);
    db.query(sqlQuery, (err, rows) => {
      if (err) {
        return failureCallback(err);
      } else {
      Queries.createAppoint(param);
        return successCallback("Created Appointment");
      }
    });
    },
  
  

  
   deleteAppoint: async (appointData) => {
    console.log("Appoint Data : ",appointData);
     appointData.id_vet_hour = +appointData.id_vet_hour;
    return new Promise((resolve, reject) => {
      let sqlQuery = `DELETE FROM appointments WHERE id_vet_hour=${appointData.id_vet_hour}`;
       db.query(sqlQuery, (err, res) => {
         if (err) reject(err);
               resolve(res);
      });
    });
  },

 
 
    cancelAppointment: (param, successCallback, failureCallback) => {  
      console.log(param);
       param.id_vet_hour = +param.id_vet_hour;
      let sqlQuery = `UPDATE veterinary_has_hour SET available=1 WHERE id_vet_hour=${param.id_vet_hour}`;
     console.log(sqlQuery);
    db.query(sqlQuery, (err, rows) => {
      if (err) {
        return failureCallback(err);
      } else {
        Queries.deleteAppoint(param);
        return successCallback("Appointment Cancelled");
      }
    });
  },
  


};

export default Queries;


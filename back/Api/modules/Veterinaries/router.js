import express from "express";
import VetController from "./controller";

const router = express.Router();


//router.post("/register", VetController.register);
//router.get("/:id", VetController.getVet)
//router.delete("/:id", VetController.delete);
//router.put("/:id", VetController.editVet);
//router.post("/authenticate", VetController.authenticate);
router.get("/", VetController.getAllVet);
router.get("/:id", VetController.getHoursByVet);
router.post("/take-appoint", VetController.takeAppointment);
router.post("/cancel-appoint", VetController.cancelAppointment);




export default router;
import Queries from "./query";

const VetServices = {


	getAllVet: (req, callback) => {
			Queries.getAllVet(req, (response) => {
					return callback({
					success: true,
					message: "All vetos",
					data: response,
				});
				},
					(error) => {
				return callback({ success: false, message: error });
			}
		);
	},


  
	
getHoursByVet: (req, callback) => {
		Queries.getHoursByVet(req,(response) => {
				return callback({
					success: true,
					message: "All availabilities of this vet",
					data: response,
				});
			},
			(error) => {
				return callback({ success: false, message: error });
			}
		);
	},

  takeAppointment: (req, callback) => {
    // console.log("Appoint Data : ", req.body);
    Queries.takeAppointment(
      req.body,
      (response) => {
        return callback({ success: true, message: response });
      },
      (error) => {
        return callback({ success: false, message: error });
      }
    );
  },

 cancelAppointment: (req, callback) => {
    Queries.cancelAppointment(
      req.body,
      (response) => {
        return callback({ success: true, message: response });
      },
      (error) => {
        return callback({ success: false, message: error });
      }
    );
  },


};

export default VetServices;
import Queries from "./query";


const RefServices = {

  getAllMotif: (req, callback) => {
    Queries.getAllMotif(req,
      response => {
        return callback({ success: true, message: response });
      },
      error => {
        return callback({ success: false, message: error });
      });
  },
 
 getAllVaccin: (req, callback) => {
    Queries.getAllVaccin(req,
      response => {
        return callback({ success: true, message: response });
      },
      error => {
        return callback({ success: false, message: error });
      });
  },


 
 


};

export default RefServices;

import TrainningServices from "./service";

const TrainningController = {

	getAll: (req, res) => {
		TrainningServices.getAll(req, (result) => {
			result.success
				? res.status(200).send(result)
				: res.status(404).send(result);
		});
	},

registerTraining: async (req, res) => {
    TrainningServices.registerTraining(req.body).then(result =>
      res.status(result.status).send(result.payload)
    );
  },



};

export default TrainningController;

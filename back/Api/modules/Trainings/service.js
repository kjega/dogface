import Queries from "./query";

const TrainningServices = {
	
getAll: (req, callback) => {
		Queries.getAll(
			req,
			(response) => {
				return callback({
					success: true,
					message: "dogs and user of the dogs retrieve",
					data: response,
				});
			},
			(error) => {
				return callback({ success: false, message: error });
			}
		);
	},


	registerTraining: (req, callback) => {
    Queries.registerTraining(
      req,
      response => {
        return callback({ success: true, message: response });
      },
      error => {
        return callback({ success: false, message: error });
      }
    )
  },



};

export default TrainningServices;

import express from "express";
import TrainningController from "./controller";

const router = express.Router();

router.get("/", TrainningController.getAll)
//router.get("/:id", TrainningController.getDogTrainning)
router.post("/", TrainningController.registerTraining)




export default router;
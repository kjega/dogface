import DogServices from "./service";
import { imageUpload } from '../../config/image-upload.json'
// var ImageKit = require("imagekit");

// var imagekit = new ImageKit({
//     publicKey : "public_sjCUqYX3imqW2vJVj51slPpS/AU=",
//     privateKey : "private_mhAA3aD1L2Vv0XXcRqwUlv6vhsc=",
//     urlEndpoint : "https://ik.imagekit.io/a4dfgmv1j6k"
// });

const DogController = {

	getAllDogsByUser: (req, res) => {
		DogServices.getAllDogsByUser(req, (result) => {
			result.success
				? res.status(200).send(result)
				: res.status(404).send(result);
		});
	},




  getDog: (req, res) => {
    DogServices.getDog(req, result => {
      result.success ? res.status(200).send(result) : res.status(404).send(result)
    })
  },


   registerDog: (req, res) => {
        DogServices.registerDog(req, result => {
            result.success
                ? res.status(201).send(result)
                : res.status(404).send(result)
        })
	},
   
  
  editDog: (req, res) => {
    DogServices.editDog(req, (result) => {
         result.success
        ? res.status(200).send(result)
        : res.status(404).send(result);
    });
  },
   
   
  addDogImage: (req, res) => {
     
      //  console.log(req.file.buffer);
    //  var base64Img = req.file.buffer;

    //      imagekit.upload({
    //                     file : base64Img, //required
    //                     fileName : "dogImg.jpg"   //required
                       
    //      }, function (error, result) {
    //        if (error) { console.log(error); }
    //        else {
    //          console.log(result.url);
    //        }
    //      })
   
    DogServices.addDogImage(req, (result) => {
      //Will be executed once the service is finished
      result.success
        ? res.status(200).send(result)
        : res.status(404).send(result);
    });
  },
   
   
 addDogImage1: (req, res) => {
  
    DogServices.addDogImage1(req, (result) => {
      //Will be executed once the service is finished
      result.success
        ? res.status(200).send(result)
        : res.status(404).send(result);
    });
  },
   







  deleteDogImage: (req, res) => {
     
   
   
    DogServices.deleteDogImage(req, (result) => {
      //Will be executed once the service is finished
      result.success
        ? res.status(200).send(result)
        : res.status(404).send(result);
    });
  },
   
   	getAllDogImages: (req, res) => {
		DogServices.getAllDogImages(req, (result) => {
			result.success
				? res.status(200).send(result)
				: res.status(404).send(result);
		});
	},

};

export default DogController;

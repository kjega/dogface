import express from "express";
import VaccinController from "./controller";

const router = express.Router();


router.post("/register", VaccinController.registerVaccin);
router.get("/:id_dog", VaccinController.getAllVaccinsByDog);



export default router;

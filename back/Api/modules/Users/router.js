import express from "express";
import UserController from "./controller";

const router = express.Router();

//router.get("/", UserController.getAllUser)
router.post("/register", UserController.register);
//router.get("/:id", UserController.getUser);
router.get("/:id", UserController.getUser1);
router.delete("/:id", UserController.delete);
router.put("/:id", UserController.editUser);
router.post("/authenticate", UserController.authenticate);




export default router;

//TODO: 
// user edit
//user authentification
//user delete
// while user delete il faut delete all lie avec ce user
// user role ici properietaire et eleveur

import UserServices from "./service";

const UserController = {
   getUser: (req, res) => {
    UserServices.getUser(req, result => {
      result.success ? res.status(200).send(result) : res.status(404).send(result)
    })
  },
  getUser1: (req, res) => {
    UserServices.getUser1(req, result => {
      result.success ? res.status(200).send(result) : res.status(404).send(result)
    })
  },
  authenticate: (req, res) => {
    UserServices.authenticate(req.body).then(result =>
      res.status(result.status).send(result.payload)
    );
  },
 
  register: async (req, res) => {
    UserServices.register(req.body).then(result => {
      // res.status(result.status).send(result.payload)
      console.log(result);
      res.json(result)
    }
    );
  },

   delete: (req, res) => {
        UserServices.delete(req, result => {
            result.success ? res.status(200).send(result) : res.status(404).send(result)
        })
    },

editUser: (req, res) => {
   
    UserServices.editUser(req, (result) => {
      //Will be executed once the service is finished
      result.success
        ? res.status(200).send(result)
        : res.status(404).send(result);
    });
  },

   
   
   

};

export default UserController;

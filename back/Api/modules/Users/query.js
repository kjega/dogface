import db, { database } from "../../setup/database";



const Queries = {

  //authentication  (comparing actual email and password)
  authenticate: (user, successCallback, failureCallback) => {
    let sqlQuery = `SELECT * FROM users WHERE email="${user.email}" AND password="${user.password}"`;
    db.query(sqlQuery, (err, rows) => {
      if (err) {
        return failureCallback(err);
      }
      if (rows.length > 0) {
        return successCallback(rows[0]);
      } else {
        return successCallback("Incorrect username or password combinaison");
      }
    });
  },
 
  register: async (users) => {
    // console.log("users : ",users);
    //convert string to int using '+'
    users.is_veterinay = +users.is_veterinay;
    return new Promise((resolve, reject) => {
      let sqlQuery = `INSERT INTO users (id_user, username, email, password,is_veterinay) VALUES (NULL,"${users.username}", "${users.email}", "${users.hashedPassword}", "${users.is_veterinay}")`;
       db.query(sqlQuery, (err, res) => {
         if (err) reject(err);
        //  console.log("ID_USER :", res.insertId);
         if (users.is_veterinay == 1)
         {
          //  console.log("is_vet");
                  let sqlQuery1 = `INSERT INTO veterinary (id_vet,id_user, address_cabinet,code_postal,longitude, latitude) VALUES (NULL,"${res.insertId}", "${users.address_cabinet}", "${users.code_postal}", "${users.longitude}", "${users.latitude}")`;
         db.query(sqlQuery1, (err, res) => {
           if (err) reject(err);
            //  return res;
           resolve(res);
         
                
            });
           }
  
         resolve(res);
      });
    });
  },

 
 
  getByUserEmail: (username,email,hashedPassword,is_veterinay,address_cabinet,code_postal,longitude,latitude) => {
       return new Promise((resolve, reject) => {
         let sqlQuery = `SELECT * FROM users WHERE email="${email}"`;
       db.query(sqlQuery, (err, res) => {
         if (err) reject(err);
         console.log("ID_USER :", res.length);
         if (res.length > 0)
         {
           resolve(res);
           
         }
         else {
              Queries.register({ username, email, hashedPassword, is_veterinay,address_cabinet,code_postal,longitude,latitude })
         //condition
         //no match  -  [] (length==0)
         //match    - [user]  (length==1)
         resolve(res);
         }
       
      });
    });
    // let sqlQuery = `SELECT * FROM users WHERE email="${email}"`;
    // db.query(sqlQuery, (err, rows) => {
    //   if (err) {
    //     console.log(err);
    //     return failureCallback(err);
    //   }
    //   if (rows.length > 0) {
    //     // console.log(rows);
    //     return rows;
    //   } else {
    //     console.log("No Match");
    //     Queries.register({ username, email, hashedPassword, is_veterinay,address_cabinet,code_postal,longitude,latitude })
    //        // return "No matching user";
    //     console.log(rows);
    //      return rows;
    //   }
    //   return rows;
    // })
  },
  
  getUser: (param, successCallback, failureCallback) => {
    let sqlQuery = `SELECT email, username FROM users WHERE id_user=${param.params.id}`;
    db.query(sqlQuery, (err, rows) => {
      if (err) {
        return failureCallback(err);
      }
      if (rows.length > 0) {
        return successCallback(rows);
      } else {
        return successCallback("No matching user");
      }
    })
  },

  
 getUser1: (param, successCallback, failureCallback) => {
    let sqlQuery = `SELECT * FROM users u INNER JOIN veterinary v ON v.id_user = u.id_user WHERE u.id_user=${param.params.id}`;
    db.query(sqlQuery, (err, rows) => {
      if (err) {
        return failureCallback(err);
      }
      if (rows.length > 0) {
        return successCallback(rows);
      } else {
        return successCallback("No matching user");
      }
    })
  },

  ///authentication (for checking user existence)
   getByEmail: (email) => {
        return new Promise((resolve, reject) => {
         let sqlQuery = `SELECT * FROM users WHERE email="${email}"`;
       db.query(sqlQuery, (err, res) => {
         if (err) reject(err);
         console.log("ID_USER :", res);
         //condition
         //no match  -  [] (length==0)
         //match    - [user]  (length==1)
         resolve(res);
      });
    });
  
  },

 

  delete: (id, successCallback, failureCallback) => {
  //  DELETE t1, t2 
// FROM table1 t1 LEFT JOIN table2 t2 ON t1.id = t2.id 
// WHERE t1.id = some_id
    //select id_dog dog where id_user={}
      let sqlQuery = `SELECT * FROM dogs where id_user=${id}`
    db.query(sqlQuery, (err, rows) => {
      if (err) {
        return failureCallback(err);
      }
      if (rows.length > 0)
      {
        console.log("ID : ",rows[0].id_dog)
    let sqlQuery1 = `DELETE FROM dog_has_trainning WHERE id_dog= ${rows[0].id_dog}`
    db.query(sqlQuery1, (err, rows) => {

      if (err) {
        return failureCallback(err);
      }
   
      if (rows.length > 0) {
            console.log("DOg table")
        let sqlQuery2 = `DELETE FROM appointments  WHERE id_user= ${id}`
        db.query(sqlQuery2, (err, rows) => {
          if (err) {
            return failureCallback(err);

          }
           let sqlQuery3 = `DELETE FROM dogs  WHERE id_user= ${id}`
        db.query(sqlQuery3, (err, rows) => {
          if (err) {
            return failureCallback(err);

          }
               let sqlQuery4 = `DELETE FROM users  WHERE id_user= ${id}`
        db.query(sqlQuery4, (err, rows) => {
          if (err) {
            return failureCallback(err);

          }
             return successCallback(rows);
        })
            //  return successCallback(rows);
        })
            //  return successCallback(rows);
        })
      } else {
            console.log("DOg table")
        let sqlQuery2 = `DELETE FROM appointments  WHERE id_user= ${id}`
        db.query(sqlQuery2, (err, rows) => {
          if (err) {
            return failureCallback(err);

          }
           let sqlQuery3 = `DELETE FROM dogs  WHERE id_user= ${id}`
        db.query(sqlQuery3, (err, rows) => {
          if (err) {
            return failureCallback(err);

          }
               let sqlQuery4 = `DELETE FROM users  WHERE id_user= ${id}`
        db.query(sqlQuery4, (err, rows) => {
          if (err) {
            return failureCallback(err);

          }
          //sqlquery5
             return successCallback(rows);
        })
            //  return successCallback(rows);
        })
              //  return successCallback(rows);
        })  
        // return successCallback("No movie with this id");
      }
    });

      }
      else {
        return successCallback(rows);
      }
    })
    
  },

  
 //i will not modifier le email ill check  
  editUser: (param, successCallback, failureCallback) => {  
    console.log(param.body.password);
    param.body.is_veterinay = +param.body.is_veterinay;
     let sqlQuery = `UPDATE users SET username="${param.body.username}", email ="${param.body.email}",password ="${param.body.password}" WHERE id_user=${param.params.id}`;
    console.log(sqlQuery);
    db.query(sqlQuery, (err, rows) => {
      if (err) {
        return failureCallback(err);
      } else {

        if (param.body.is_veterinay == 1)
        {
           let sqlQuery1 = `UPDATE veterinary SET address_cabinet="${param.body.address_cabinet}", code_postal ="${param.body.code_postal}" WHERE id_user=${param.params.id}`;
          db.query(sqlQuery1, (err, rows) => {
            if (err) {
              return failureCallback(err);
            } else {
                 return successCallback("Profil modifier");
            }
          })
          }
        // return successCallback("Profil modifier");
      }
    });
  },


};


export default Queries;

import express from "express";
import usersRoutes from "../modules/Users/router";
// import homeRoutes from "../Home/router";
import dogRoutes from "../modules/Dogs/router";
import trainningRoutes from "../modules/Trainings/router";
import vaccinRoutes from "../modules/Vaccins/router";
import vetRoutes from "../modules/Veterinaries/router"
import appointRoutes from "../modules/Appoiments/router"
import packRoutes from "../modules/Packages/router"
import lessonRoutes from "../modules/Lesson/router"
import refRoutes from "../modules/Reference/router"




const Router = (server) => {
	
	server.use("/api/vet", vetRoutes);
	server.use("/api/user", usersRoutes);
	// server.use("/api/home", homeRoutes);
	server.use("/api/dog", dogRoutes);
	server.use("/api/trainning", trainningRoutes);
	server.use("/api/vaccin", vaccinRoutes);
	server.use("/api/appoint", appointRoutes);
	server.use("/api/pack", packRoutes);
	server.use("/api/lesson", lessonRoutes);
	server.use("/api/ref", refRoutes);

};

export default Router;
